package DAOp2;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.Test;

public class DAOImpTest {

	@Test
	public void inserttest() throws SQLException {
		DAOImp daoimp = new DAOImp();
		ConnectionManager connect = new ConnectionManager();
		//daoimp.insert(connect.getconnection(),7,"Nine Chen","0","1970-06-06");
		daoimp.query(connect.getconnection(), 7);
		Statement st = connect.getconnection().createStatement();
		ResultSet rs2 = st.executeQuery("SELECT * FROM profile WHERE ID=" + 7);
		rs2.last();
		assertEquals(7,rs2.getInt(1));
	}
	
	@Test
	public void checkidtest() throws SQLException{
		DAOImp daoimp2 = new DAOImp();
		ConnectionManager connect2 = new ConnectionManager();
		assertTrue(daoimp2.checkid(connect2.getconnection(), 9, "insert"));	
	}

}
