package DAOp2;

import java.sql.DriverManager;
import java.sql.SQLException;

import com.mysql.jdbc.Connection;

public class ConnectionManager {
	final static String DRIVER = "com.mysql.jdbc.Driver"; //The type name of JDBC driver  
	final static String URL = "jdbc:mysql://localhost/Personal_Data"; //URL of DB
   final static String USER = "root";
   final static String PASS = "123456";
   
   public Connection getconnection() throws SQLException{
	   try {
		Class.forName(DRIVER);
	} catch (ClassNotFoundException e) {
		System.out.println("Can't connect to MySQL" + e.getMessage());
		e.printStackTrace();
		System.exit(0);		
	}
	   Connection con = (Connection) DriverManager.getConnection(URL,USER,PASS); 
	return con;
	   
   }
   
   public void connectOFF(Connection conn) {
		try {
			conn.close();
			if (conn.isClosed()) {
				System.out.println("The database is closed");
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}
   
   
   
}
