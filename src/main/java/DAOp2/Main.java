package DAOp2;

import java.sql.SQLException;

public class Main {

public static void main(String[] args) throws SQLException{

		DAOImp action = new DAOImp ();
		ConnectionManager con1 = new  ConnectionManager();
		Modelobject modelobject = (args.length>2) ? 
				new Modelobject(args[0],Integer.parseInt(args[1]),args[2],args[3],args[4]) : 
				new Modelobject(args[0],Integer.parseInt(args[1]));
		
		if(!action.checkid(con1.getconnection(), modelobject.getID(), modelobject.getaction())){
			System.exit(0);
		};
		switch(args[0]){
		case "insert" :
			action.insert(con1.getconnection(), modelobject.getID(), modelobject.getName(), modelobject.getsex(), modelobject.getbirth());
			break;
		case "delete":
			action.delete(con1.getconnection(), modelobject.getID());
			break;
		case "query":
			action.query(con1.getconnection(),modelobject.getID());
			break;
		case "update":
			action.update(con1.getconnection(), modelobject.getID(), modelobject.getName(), modelobject.getsex(), modelobject.getbirth());
			break;
		default:
			System.out.println("Wrong argument");
			System.exit(0);
		}
		
		action.showDB(con1.getconnection());
		con1.connectOFF(con1.getconnection());
   }

}
