package DAOp2;


import java.util.ArrayList;

public interface DAO {
	public void insert(com.mysql.jdbc.Connection con,int ID,String name,String sex,String birth);
	public void delete(com.mysql.jdbc.Connection con,int ID);
	public void update(com.mysql.jdbc.Connection con,int ID,String name,String sex,String birth);
	public ArrayList<String> query(com.mysql.jdbc.Connection con,int ID);

}
	
