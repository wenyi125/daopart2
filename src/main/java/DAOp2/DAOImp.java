package DAOp2;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.PreparedStatement;

public class DAOImp implements DAO {

	@Override
	public void insert(Connection con, int ID, String name, String sex, String birth) {
		String query = " INSERT INTO profile (ID, name, sex, birth)" + " values (?, ?, ?, ?)";
		PreparedStatement preparedStmt;
		try {
			preparedStmt = (PreparedStatement) con.prepareStatement(query);
			preparedStmt.setInt(1, ID);
			preparedStmt.setString(2, name);
			preparedStmt.setString(3, sex);
			preparedStmt.setDate(4, Date.valueOf(birth));
			preparedStmt.execute();
			System.out.println("Success!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void delete(Connection con, int ID) {
		String query = "DELETE FROM profile WHERE ID = ?";
		PreparedStatement preparedStmt;
		try {
			preparedStmt = (PreparedStatement) con.prepareStatement(query);
			preparedStmt.setInt(1, ID);
			preparedStmt.execute();
			System.out.println("Success!");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void update(com.mysql.jdbc.Connection con, int ID, String name, String sex, String birth) {
		String query = "UPDATE profile SET name = ?, sex = ?, birth = ? WHERE ID = ?";
		PreparedStatement preparedStmt;
		try {
			preparedStmt = (PreparedStatement) con.prepareStatement(query);
			preparedStmt.setString(1, name);
			preparedStmt.setString(2, sex);
			preparedStmt.setDate(3, Date.valueOf(birth));
			preparedStmt.setInt(4, ID);
			preparedStmt.executeUpdate();
			System.out.println("Success!");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public ArrayList<String> query(com.mysql.jdbc.Connection con, int ID) {
		ArrayList<String> list = null;
		try {
			Statement st = con.createStatement();
			ResultSet rs2 = st.executeQuery("SELECT * FROM profile WHERE ID=" + ID);
			rs2.last();
			list = new ArrayList<String>();
			list.add(rs2.getString("ID"));
			list.add(rs2.getString("name"));
			list.add(rs2.getString("sex"));
			list.add(rs2.getString("birth"));
			for (int i = 0; i < list.size(); i++)
				System.out.print(list.get(i) + " ");
			System.out.println();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return list;
	}
	
	public void showDB (Connection con){
		try {
			Statement smt = con.createStatement();
			ResultSet rs = smt.executeQuery("SELECT * FROM profile");
			while(rs.next()){
				String s = rs.getString(1)+rs.getString(2)+rs.getString(3)+rs.getString(4);
				System.out.println(s);	
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}	
	}
	
	public Boolean checkid(Connection con,int ID, String action) {
		boolean idrepeat = false;
		try {
			Statement st = con.createStatement();
			ResultSet rs = st.executeQuery("SELECT * FROM profile");
			while (rs.next()) {
				if (String.valueOf(ID).equals(rs.getString("ID")))
					idrepeat = true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}

		if (action.equals("insert") && idrepeat == true) {
			System.out.println("The ID is reapted");
			return false;
		}
		if (!action.equals("insert") && idrepeat == false) {
			System.out.println("The ID is not exist");
			return false;
		}
		return true;
	}

}
